<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register()
    {
        # code...
        return view('register');
    }

    public function welcome(Request $data)
    {
        # code...
        return view('welcome', $data);
    }
}
