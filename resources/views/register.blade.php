<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>

<body>
    <form method="post" action="/welcome">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h4>Sign Up Form</h4>

        <label for="f_name">First Name:</label><br><br>
        <input type="text" id="f_name" name="f_name"><br><br>

        <label for="f_name">Last Name:</label><br><br>
        <input type="text" id="l_name" name="l_name"><br><br>

        Gender:<br><br>
        <input type="radio" id="male" name="gender" value="Male"><label for="male">Male</label><br><br>
        <input type="radio" id="female" name="gender" value="Female"><label for="female">Female</label><br><br>
        <input type="radio" id="other" name="gender" value="Other"><label for="other">Other</label><br><br>

        <label for="nationality">Nationality:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="english">English</option>
            <option value="chinese">Chinese</option>
            <option value="japanese">Japanese</option>
        </select><br><br>

        Language Spoken:<br><br>
        <input name="lang" type="checkbox" id="id-ID" value="Bahasa Indonesia"><label for="id-ID">Bahasa Indonesia</label><br>
        <input name="lang" type="checkbox" id="en-US" value="English"><label for="en-US">English</label><br>
        <input name="lang" type="checkbox" id="ja-JP" value="Other"><label for="ja-JP">Japanese</label><br>
        <input name="lang" type="checkbox" id="oth" value="Other"><label for="oth">Other</label><br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <button type="submit" name="submit">Sign Up</button>
    </form>
</body>

</html>